package com.twuc.webApp;

import com.twuc.webApp.entity.Reservations;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ReservationsRepo extends JpaRepository<Reservations,Long> {

}
