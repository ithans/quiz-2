package com.twuc.webApp;

import com.twuc.webApp.entity.Persona;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface PersonRepo extends JpaRepository<Persona,Long> {
}
