package com.twuc.webApp.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import java.time.Instant;

@Entity
public class Reservations {
    @Id
    @GeneratedValue
    private Long id;
    @Column(nullable = false,length = 128)
    private String username;
    @Column(nullable = false,length = 64)
    private String companyName;
    @Column(nullable = false)
    private String zoneId;
    @Column(nullable = false)
    private Instant startTime;
    @Column(nullable =false)
    private String duration;

    public Reservations(String username, String companyName, String zoneId, Instant startTime, String duration) {
        this.username = username;
        this.companyName = companyName;
        this.zoneId = zoneId;
        this.startTime = startTime;
        this.duration = duration;
    }

    public Reservations() {
    }

    public Long getId() {
        return id;
    }

    public String getUsername() {
        return username;
    }

    public String getCompanyName() {
        return companyName;
    }

    public String getZoneId() {
        return zoneId;
    }

    public Instant getStartTime() {
        return startTime;
    }

    public String getDuration() {
        return duration;
    }
}
