package com.twuc.webApp.web;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.twuc.webApp.PersonRepo;
import com.twuc.webApp.ReservationsRepo;
import com.twuc.webApp.entity.Persona;
import com.twuc.webApp.entity.Reservations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.swing.text.html.parser.Entity;
import java.util.List;
import java.util.Optional;
import java.util.TimeZone;

@RestController
@RequestMapping("/api")
public class StaffController {
    @Autowired
    private PersonRepo personRepo;
    @Autowired
    private ReservationsRepo reservationsRepo;
    @PostMapping("/staffs")
    public ResponseEntity saveStaff(@RequestBody Persona persona){
        Persona res = personRepo.save(persona);
        personRepo.flush();
        if (! personRepo.findById(res.getId()).isPresent()){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }
        return ResponseEntity.status(HttpStatus.CREATED)
                .header("Location","/api/staffs/"+res.getId())
                .build();
    }

    @GetMapping("/staffs/{staffId}")
    public ResponseEntity getStaff(@PathVariable Long staffId){
        Optional<Persona> staffOp = personRepo.findById(staffId);
        Persona persona = null;
        if (staffOp.isPresent()){
            persona = staffOp.get();
        }else{
            return ResponseEntity.status(HttpStatus.NOT_FOUND).build();
        }
        return ResponseEntity.status(HttpStatus.OK)
                .contentType(MediaType.APPLICATION_JSON)
                .body(persona);
    }

    @GetMapping("/staffs")
    public List<Persona> getAllStaffs(){
        List<Persona> personasList = personRepo.findAll();
        int size = personasList.size();
        System.out.println(size);
        return personasList;

    }

    @PutMapping("/staffs/{staffId}/timezone")
    public ResponseEntity setTimeZone(@PathVariable Long staffId ,@RequestBody Persona timeZone){
        Optional<Persona> staffOp = personRepo.findById(staffId);
        Persona persona = staffOp.get();
        if (timeZone.getZoneId()==null){
            return ResponseEntity.status(HttpStatus.BAD_REQUEST).build();
        }
        persona.setZoneId(timeZone.getZoneId());
        Persona result = personRepo.save(persona);
        personRepo.flush();
        return ResponseEntity.status(HttpStatus.OK)
                .body(result);
    }

    @GetMapping("/timezones")
    public ResponseEntity getAllTimezone() throws JsonProcessingException {
        ObjectMapper mapper = new ObjectMapper();
        String[] avilableTimezones = TimeZone.getAvailableIDs();
        String timeZones = mapper.writeValueAsString(avilableTimezones);
        return ResponseEntity.status(HttpStatus.OK)
                .contentType(MediaType.APPLICATION_JSON)
                .body(timeZones);
    }

//    @PostMapping("/staffs/{staffId}/reservations")
//    public ResponseEntity addResvations(@PathVariable Long staffId, @RequestBody Reservations reservations) {
//        Reservations reservation = reservationsRepo.save(reservations);
//        return null;
//    }
}
